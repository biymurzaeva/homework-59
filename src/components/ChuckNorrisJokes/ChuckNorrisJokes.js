import React, {useEffect, useState} from 'react';
import Joke from "../Joke/Joke";
import './ChuckNorrisJokes.css';

const ChuckNorrisJokes = () => {
	const url = 'https://api.chucknorris.io/jokes/random';

	const [jokes, setJokes] = useState([]);

	useEffect(() => {
		const fetchData = async () => {
			for (let i = 0; i < 10; i++) {
				const response = await fetch(url);

				if (response.ok) {
					const getJoke = await response.json();
					setJokes(prev => [...prev, getJoke]);
				}
			}
		};

		fetchData().catch(e => console.error(e));
	}, []);
	return (
		<>
			<div className="Jokes">
				{jokes.map(joke => (
					<Joke
						key={joke.id}
						title={joke.value}
						author='Chuck Norris '
					/>
				))}
			</div>
		</>
	);
};

export default ChuckNorrisJokes;