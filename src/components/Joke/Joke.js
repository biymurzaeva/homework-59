import React from 'react';
import './Joke.css';

class Joke extends React.Component {
	render() {
		const {title, author} = this.props;

		return (
			<div>
				<article className="Joke">
					<h1>{title}</h1>
					<div className="Info">
						<div className="Author">{author}</div>
					</div>
				</article>
			</div>
		);
	}
}

export default Joke;