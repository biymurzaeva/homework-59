import React, {Component} from 'react';
import './Film.css';

class Film extends Component {
	shouldComponentUpdate(nextProps) {
		return nextProps.value !== this.props.value
	}

	render() {
		const {value, changeValue, removeFilm} = this.props;
		return (
			<div className="Film">
				<input
					type="text"
					value={value}
					onChange={changeValue}
				/>
				<button onClick={removeFilm}>x</button>
			</div>
		);
	}
}

export default Film;