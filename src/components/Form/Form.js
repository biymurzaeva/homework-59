import {Component} from 'react';
import Film from "../Film/Film";
import './Form.css';

class Form extends Component {
	state = {
		film: '',
		films: []
	};

	setFilm = (name) => {
		this.setState({
			film: name,
		});
	}

	addFilm = (e) => {
		e.preventDefault();

		this.state.films.push({name: this.state.film, id: Math.floor(Date.now() / 1000)});

		this.setState( {
			film: '',
		});
	}

	changeFilm = (newName, id) => {
		this.setState({
			films: this.state.films.map(f => {
				if (f.id === id) {
					return {...f, name: newName};
				}

				return f;
			})
		});
	}

	removeFilm = (id) => {
			this.setState({
				films: this.state.films.filter(f => f.id !== id)
			});
	}

	render() {
		return (
			<div className="Main">
				<div className="Form">
					<form onSubmit={this.addFilm}>
						<input
							type="text"
							value={this.state.film}
							onChange={e => this.setFilm(e.target.value)}
						/>
						<button type="submit">Add</button>
					</form>
				</div>
				<h4>To watch list:</h4>
				{this.state.films.map(film => (
					<Film
						key={film.id}
						value={film.name}
						changeValue={e => this.changeFilm(e.target.value, film.id)}
						removeFilm={() => this.removeFilm(film.id)}
					/>
				))}
			</div>
		);
	}
}

export default Form;